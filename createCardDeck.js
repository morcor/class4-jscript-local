/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */

// function createCard(val,displayVal,suit) {
// 	return {
// 	  val,
// 	  displayVal,
// 	  suit,
// 	};
//   }
//   const getDeck = () => {
// 	let completeDeck = []
// 	const suits = ['hearts', 'spades', 'clubs', 'diamonds']
// 	suits.forEach((suit) => {
// 	  // create card objects
// 	  // count to 13
// 	  const numCards = 13
// 	  // check the index of that count
// 	  for (let index = 1; index <= numCards; index++) {
// 		if (index === 1) {
// 		  completeDeck.push(createCard(11,'Ace',suit))
// 		}
// 		else if (index > 1 && index < 11) {
// 		  completeDeck.push(createCard(index,index.toString(),suit))
// 		}
// 		else if (index === 11) {
// 		  completeDeck.push(createCard(10,'Jack',suit)); 
// 		}
// 		else if (index === 12) {
// 		  completeDeck.push(createCard(10,'Queen',suit)); 
// 		}
// 		else if (index === 13) {
// 		  completeDeck.push(createCard(10,'King',suit)); 
// 		}
		
// 	  }
// 	})
// 	return completeDeck;
// }
//

const getDeck = () => {
	let deck = [];
	let suits = ['Hearts', 'Clubs', 'Diamonds', 'Spades']
	let faces = ["Jack", "Queen", "King", "Ace"]

	for (let i = 0; i < suits.length; i++) {

		for (let j = 2; j <= 10; j++)  {
			let newCard = {
				val: j,
				displayVal: j.toString(),
				suit: suits[i],
			}
			// console.log("the new card is: ", newCard)
			deck.push(newCard)
		}
		for (let face = 0; face < faces.length; face++) {
			let val = faces[face] === "Ace" ? 11 : 10
			let newCard = {
				val,
				displayVal: faces[face],
				suit: suits[i]
			}
			// console.log("the new card is: ", newCard)
			deck.push(newCard)
		}
		
		// console.log("deck so far", deck)

	}
	return deck;
};


// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];
console.log(randomCard);

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);