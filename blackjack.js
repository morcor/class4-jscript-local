const blackjackDeck = getDeck();
  // console.log(blackjackDeck);

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {
  constructor(name) {
    this.name = name;
    this.hand = [];
  }

  drawCard() {
    const newCard =
      blackjackDeck[Math.floor(Math.random() * blackjackDeck.length)];
    console.log(`${this.name} drew a ${newCard.displayVal} of ${newCard.suit}`);
    this.hand.push(newCard);
    }
  };
  


// // CREATE TWO NEW CardPlayers
const dealer = new CardPlayer("Jarod");
// dealer.drawCard(); //returns players drawn card
const player = new CardPlayer("Morgan");
// player.drawCard(); //returns players drawn card




/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */

const calcPoints = (hand) => {
  const blackJackScore = {
    total: 0,
    isSoft: false
  };

  hand.forEach((card) => {
    
    if (card.displayVal === "Ace" && blackJackScore.total > 10) {
    blackJackScore.total += 1;
    } else if (card.displayVal === "Ace") {
      blackJackScore.isSoft = true;
      blackJackScore.total += card.val;
    } else {
      blackJackScore.total += card.val;
    }
  });

  console.log(blackJackScore);

  return blackJackScore;
};



  
  // CREATE FUNCTION HERE

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
// CREATE FUNCTION HERE
  let draw = false;
  let hand = calcPoints(dealerHand); //blackJackScore object

  if (hand.score <= 16 || (hand.score === 17 && hand.isSoft)) { 
    draw = true;
  }

  return draw;

};

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  // CREATE FUNCTION HERE
    return `Player has ${playerScore} and Dealer has ${dealerScore}`
};

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`;
};

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  console.log(
    `${player.name}'s hand is ${displayHand.join(", ")} (${
      calcPoints(player.hand).total
    })`
  );
};

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  console.log(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
};
console.log(startGame());